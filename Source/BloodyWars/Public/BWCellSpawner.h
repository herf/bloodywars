#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BWCell.h"
#include "BWCellSpawner.generated.h"

USTRUCT()
struct FBWCellSpawnerProperties
{
    GENERATED_BODY();
    UPROPERTY(EditAnywhere)
    TSubclassOf<ABWCell> CellClass;
    UPROPERTY(EditAnywhere)
    float MinSpawnTime;
    UPROPERTY(EditAnyWhere)
    float MaxSpawnTime;
    UPROPERTY(EditAnywhere)
    FVector Extents;
    UPROPERTY(EditAnywhere)
    uint32 Count;
};

UCLASS()
class BLOODYWARS_API ABWCellSpawner : public AActor
{
	GENERATED_BODY()
    public:	
	    ABWCellSpawner();
        void SetProperties(const FBWCellSpawnerProperties& InProperties);

    protected:
	    virtual void BeginPlay() override;
        virtual void Tick(float DeltaTime) override;

    private:	
        void CalculateNextSpawnTime();
        bool CheckProperties() const;
        UWorld* SafeGetWorld();
        FVector CalculateLocation() const;

        UPROPERTY()
        USceneComponent* Transform;
        UPROPERTY(EditAnywhere)
        FBWCellSpawnerProperties Properties;
        float NextSpawnTime;
        uint32 SpawnCounter;
};
