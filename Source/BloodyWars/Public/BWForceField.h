// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BWForceField.generated.h"

class UBoxComponent;

UCLASS()
class BLOODYWARS_API ABWForceField : public AActor
{
	GENERATED_BODY()
    public:	
	    ABWForceField();
        void SetForces(const FVector& InBloodForce, const FVector& InVirusForce);

    protected:
	    // Called when the game starts or when spawned
	    virtual void BeginPlay() override;

    private:
        UFUNCTION()
        void OnActorEnterOverlap(AActor* OverlappedActor, AActor* OtherActor);

        UPROPERTY()
        UBoxComponent* TriggerBox;

        FVector BloodForce;
        FVector VirusForce;
};
