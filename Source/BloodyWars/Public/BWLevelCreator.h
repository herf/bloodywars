#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BWCellSpawner.h"
#include "BWLevelCreator.generated.h"

class UInstancedStaticMeshComponent;

USTRUCT()
struct FCellSpawnerKey
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere)
    FVector Position;
    UPROPERTY(EditAnywhere)
    TSubclassOf<ABWCellSpawner> SpawnerClass;

    bool operator== (const FCellSpawnerKey& Other)
    {       
        return Other.Position == Position && Other.SpawnerClass == SpawnerClass;
    }
    friend uint32 GetTypeHash(const FCellSpawnerKey& Other)
    {
        return GetTypeHash(MakeTuple(Other.Position, Other.SpawnerClass));
    }
};

USTRUCT()
struct FCorners
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere)
    TArray<FVector> Corners;
};

UCLASS()
class BLOODYWARS_API ABWLevelCreator : public AActor
{
	GENERATED_BODY()
    public:	
	    ABWLevelCreator();

    protected:
	    virtual void BeginPlay() override;
        void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    private:
        bool CheckProperties() const;
        bool GenerateLevel(const FVector& From, const FVector& To);
        void BuildLevel();
        void CreateSpawners() const;
        void AddToLevel(const FVector& Position);
        void CreateVein(const FVector& Position);
        void CreateForceField(const FVector& Position,
                              const FVector& PreviousPosition,
                              const FVector& NextPosition);

        UFUNCTION()
        void OnRep_ForceFieldsArray();

        UPROPERTY(EditAnywhere, Category = "Level|Vein")
        TArray<FVector> Corners;
        UPROPERTY(EditAnywhere, Category = "Level|Vein")
        TArray<FCorners> Branches;
        UPROPERTY(EditDefaultsOnly, Category = "Level|Vein")
        UInstancedStaticMeshComponent* VeinInstancedMesh;
        // TODO: Add a Property for the forcefield class as well
        UPROPERTY(EditAnywhere, Category = "Level|Spawnpoints")
        TMap<FCellSpawnerKey, FBWCellSpawnerProperties> CellSpawners;
        UPROPERTY(ReplicatedUsing=OnRep_ForceFieldsArray)
        TArray<FVector> ForceFieldsArray;
        
        TSet<FVector> Level;
        TSet<FVector> Vein;
        TSet<FVector> ForceFields;
};
