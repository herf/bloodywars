#pragma once
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BWCell.generated.h"

class UStaticMeshComponent;

UCLASS()
class BLOODYWARS_API ABWCell: public AActor
{
	GENERATED_BODY()
    public:	
	    ABWCell();
        void SetForce(const FVector& InForce);

    protected:
	    virtual void BeginPlay() override;
        virtual void Tick(float DeltaTime) override;

    private:
        bool CheckProperties() const;
        FVector AlignForce(const FVector& InForce, const FVector& InDirection) const;

        UPROPERTY(EditDefaultsOnly)
        UStaticMeshComponent* CellMesh;
        UPROPERTY(EditDefaultsOnly)
        float Speed;

        FVector Force;
        FVector AngularVelocity;
};
