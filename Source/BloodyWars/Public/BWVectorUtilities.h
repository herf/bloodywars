#pragma once
#include "CoreMinimal.h"
#include <functional>

class BLOODYWARS_API BWVectorUtilities
{
    public:
        using CallbackType = std::function<void(const FVector&)>;
        static FVector Round(const FVector& Vector, int To);
        static void Neighbouring(const FVector& Position, float Distance, const CallbackType& Callback, float Step = 1);
    private:
        BWVectorUtilities() = delete;
        ~BWVectorUtilities() = delete;
        static int RoundTo(float Value, int To);
};
