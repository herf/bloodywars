// Fill out your copyright notice in the Description page of Project Settings.
#pragma once
#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogBW, Log, All);

#define BW_LOG(Actor, Verbosity, Format, ...) \
    { \
        if(Actor->HasAuthority()) \
        { \
            UE_LOG(LogBW, Verbosity, TEXT("[SERVER]: ") TEXT(__FUNCTION__) TEXT(" -- ") Format, ##__VA_ARGS__); \
        } \
        else \
        { \
            UE_LOG(LogBW, Verbosity, TEXT("[CLIENT]: ") TEXT(__FUNCTION__) TEXT(" -- ") Format, ##__VA_ARGS__); \
        } \
    }

#define BW_LOG_METHOD(Actor) \
    { \
        if(Actor->HasAuthority()) \
        { \
            UE_LOG(LogBW, Log, TEXT("[SERVER]: ") TEXT(__FUNCTION__)); \
        } \
        else \
        { \
            UE_LOG(LogBW, Log, TEXT("[CLIENT]: ") TEXT(__FUNCTION__)); \
        } \
    }

//#define UE_LOG(CategoryName, Verbosity, Format, ...) \
//	{ \
//		static_assert(IS_TCHAR_ARRAY(Format), "Formatting string must be a TCHAR array."); \
//		static_assert((ELogVerbosity::Verbosity & ELogVerbosity::VerbosityMask) < ELogVerbosity::NumVerbosity && ELogVerbosity::Verbosity > 0, "Verbosity must be constant and in range."); \
//		CA_CONSTANT_IF((ELogVerbosity::Verbosity & ELogVerbosity::VerbosityMask) <= ELogVerbosity::COMPILED_IN_MINIMUM_VERBOSITY && (ELogVerbosity::Warning & ELogVerbosity::VerbosityMask) <= FLogCategory##CategoryName::CompileTimeVerbosity) \
//		{ \
//			UE_LOG_EXPAND_IS_FATAL(Verbosity, PREPROCESSOR_NOTHING, if (!CategoryName.IsSuppressed(ELogVerbosity::Verbosity))) \
//			{ \
//				FMsg::Logf_Internal(__FILE__, __LINE__, CategoryName.GetCategoryName(), ELogVerbosity::Verbosity, Format, ##__VA_ARGS__); \
//				UE_LOG_EXPAND_IS_FATAL(Verbosity, \
//					{ \
//						_DebugBreakAndPromptForRemote(); \
//						FDebug::AssertFailed("", __FILE__, __LINE__, Format, ##__VA_ARGS__); \
//						CA_ASSUME(false); \
//					}, \
//					PREPROCESSOR_NOTHING \
//				) \
//			} \
//		} \
//	}

