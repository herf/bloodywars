// Fill out your copyright notice in the Description page of Project Settings.
#include "BWCellSpawner.h"
#include "BloodyWars.h"
#include <Engine/World.h>

ABWCellSpawner::ABWCellSpawner()
{
	PrimaryActorTick.bCanEverTick = true;
    Properties.MinSpawnTime = 1.0f;
    Properties.MaxSpawnTime = 5.0f;
    Properties.Extents = FVector(300);
    Properties.Count = 100;
    SetReplicates(false);
    if (nullptr == RootComponent)
    {
        // Just create a SceneComponent, so the Actor can have a transform
        Transform = CreateDefaultSubobject<USceneComponent>(TEXT("Transform"));
        RootComponent = Transform;
    }
    SpawnCounter = 0;
}

void ABWCellSpawner::BeginPlay()
{
	Super::BeginPlay();
    if (!CheckProperties())
    {
        // TODO: BeginPlay gets invoked before we have a chance to set the properties
        //Destroy();

        return;
    }
    CalculateNextSpawnTime();
}

void ABWCellSpawner::SetProperties(const FBWCellSpawnerProperties& InProperties)
{
    Properties = InProperties;
    if (!CheckProperties())
    {
        Destroy();

        return;
    }
    CalculateNextSpawnTime();
}

// Called every frame
void ABWCellSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    UWorld* World = SafeGetWorld();

    if (SpawnCounter >= Properties.Count)
    {
        // We are done, we can die!
        Destroy();

        return;
    }
    if (nullptr == World)
    {
        return;
    }
    if (World->GetTimeSeconds() >= NextSpawnTime)
    {
        // Time to spawn a Cell
        FTransform CellTransform;

        CalculateNextSpawnTime();
        CellTransform.SetLocation(CalculateLocation());
        World->SpawnActor(Properties.CellClass, &CellTransform);
        ++SpawnCounter;
        BW_LOG(this, Log, TEXT("Spawned a new %s at %s"), *Properties.CellClass->GetName(), *CellTransform.GetLocation().ToString());
    }
}

void ABWCellSpawner::CalculateNextSpawnTime()
{
    UWorld* World = SafeGetWorld();

    if (nullptr == World)
    {
        return;
    }
    NextSpawnTime = World->GetTimeSeconds() + FMath::RandRange(Properties.MinSpawnTime, Properties.MaxSpawnTime);
}

bool ABWCellSpawner::CheckProperties() const
{
    if (!Properties.CellClass)
    {
        BW_LOG(this, Error, TEXT("No CellClass to spawn specified!"));

        return false;
    }
    if (Properties.MinSpawnTime > Properties.MaxSpawnTime)
    {
        BW_LOG(this, Error, TEXT("Minimum spawn timeout cannot be bigger than the maximum!"));

        return false;
    }
    if (Properties.Extents.X < 0 || Properties.Extents.Y < 0 || Properties.Extents.Z < 0)
    {
        BW_LOG(this, Error, TEXT("Extents can only be positive numbers, or zeroes"));

        return false;
    }

    return true;
}

UWorld* ABWCellSpawner::SafeGetWorld()
{
    UWorld* World = GetWorld();

    if (nullptr == World)
    {
        BW_LOG(this, Error, TEXT("No World found!"));
        Destroy();
    }

    return World;
}

FVector ABWCellSpawner::CalculateLocation() const
{
    FVector Random = FVector(FMath::RandRange(0.0f, Properties.Extents.X),
                             FMath::RandRange(0.0f, Properties.Extents.Y),
                             FMath::RandRange(0.0f, Properties.Extents.Z));
    
    return GetActorLocation() + Random;
}
