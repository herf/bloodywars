// Fill out your copyright notice in the Description page of Project Settings.
#include "BWForceField.h"
#include "BWCell.h"
#include "BWBlood.h"
#include "BWVirus.h"
#include "Components/BoxComponent.h"
#include "BloodyWars.h"

ABWForceField::ABWForceField()
{
	PrimaryActorTick.bCanEverTick = false;
    SetReplicates(false);
    TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
    if (nullptr == RootComponent)
        RootComponent = TriggerBox;
    else
        TriggerBox->SetupAttachment(RootComponent);
    TriggerBox->SetCollisionProfileName(TEXT("ForceFieldDefaults"));
    TriggerBox->bGenerateOverlapEvents = true;
    TriggerBox->SetBoxExtent(FVector(350.0f));
    OnActorBeginOverlap.AddDynamic(this, &ABWForceField::OnActorEnterOverlap);
}

void ABWForceField::SetForces(const FVector & InBloodForce, const FVector & InVirusForce)
{
    BloodForce = InBloodForce;
    VirusForce = InVirusForce;
}

// Called when the game starts or when spawned
void ABWForceField::BeginPlay()
{
	Super::BeginPlay();
}

void ABWForceField::OnActorEnterOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
    ABWCell* Cell = Cast<ABWCell>(OtherActor);

    if (nullptr == Cell)
    {
        BW_LOG(this, Log, TEXT("Something else than a Cell overlapped, so ignoring it"));

        return;
    }
    if (Cell->IsA<ABWBlood>())
    {
        Cell->SetForce(BloodForce);
    }
    else if (Cell->IsA<ABWVirus>())
    {
        Cell->SetForce(VirusForce);
    }
    else
    {
        BW_LOG(this, Warning, TEXT("Unknown Cell type: %s"), *(Cell->GetClass()->GetName()));
    }
}
