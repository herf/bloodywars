#include "BWVectorUtilities.h"
#include "Math/UnrealMathUtility.h"

FVector BWVectorUtilities::Round(const FVector& Vector, int To)
{
    return FVector(RoundTo(Vector.X, To), RoundTo(Vector.Y, To), RoundTo(Vector.Z, To));
}

void BWVectorUtilities::Neighbouring(const FVector& Position, float Distance, const CallbackType& Callback, float Step)
{
    for (float X = Position.X - Distance; X <= Position.X + Distance; X += Step)
    {
        for (float Y = Position.Y - Distance; Y <= Position.Y + Distance; Y += Step)
        {
            for (float Z = Position.Z - Distance; Z <= Position.Z + Distance; Z += Step)
            {
                Callback(FVector(X, Y, Z));
            }
        }
    }
}

int BWVectorUtilities::RoundTo(float Value, int To)
{
    int IntValue = FMath::Abs(static_cast<int>(Value));
    int Modulo = IntValue % To;

    if (Modulo < To / 2)
        IntValue -= Modulo;
    else
        IntValue += (To - Modulo);
    if (Value < 0)
        IntValue = -IntValue;

    return IntValue;
}
