// Fill out your copyright notice in the Description page of Project Settings.
#include "BWLevelCreator.h"
#include "BWVectorUtilities.h"
#include "BloodyWars.h"
#include "Engine/World.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Net/UnrealNetwork.h"
#include "BWForceField.h"

// TODO: When a Raycast hits an instanced static mesh, when we get back this actor as the actor
//       And the Instanced Static MEsh component as the component. The index is also provided for the hit mesh
// TODO: Remove any literal constant and make them properties instead
// TODO: Implement adding Spanwers - including checking their properties

ABWLevelCreator::ABWLevelCreator()
{
    PrimaryActorTick.bCanEverTick = false;
    SetReplicates(true);
    SetReplicateMovement(false);
    bNetLoadOnClient = true;
    VeinInstancedMesh = CreateDefaultSubobject<UInstancedStaticMeshComponent>(TEXT("Vein Instanced Mesh"));
    if (RootComponent == nullptr)
        RootComponent = VeinInstancedMesh;
    else
        VeinInstancedMesh->SetupAttachment(RootComponent);
}

void ABWLevelCreator::BeginPlay()
{
    BW_LOG_METHOD(this);
	Super::BeginPlay();
    // We should only generate the level on the server
    if (Role != ROLE_Authority)
    {
        BW_LOG(this, Log, TEXT("Running on client! Waiting for server sending the level data!"));

        return;
    }
    BW_LOG(this, Log, TEXT("Running on server! Start generating level!"));
    if (CheckProperties() == false)
    {
        Destroy();

        return;
    }
    for (size_t Index = 0; Index < Corners.Num() - 1; ++Index)
    {
        const FVector& From = Corners[Index];
        const FVector& To = Corners[Index + 1];

        if (GenerateLevel(From, To) == false)
        {
            BW_LOG(this, Warning, TEXT("Could not generate level from %s to %s (with indices %d to %d)!"), *From.ToString(), *To.ToString(), Index, Index + 1);
            Destroy();

            return;
        }
        BW_LOG(this, Log, TEXT("Done generating level between %s and %s (with indices %d to %d)!"), *From.ToString(), *To.ToString(), Index, Index + 1);
    }
    for (const FCorners& Branch : Branches)
    {
        for (size_t Index = 0; Index < Branch.Corners.Num() - 1; ++Index)
        {
            const FVector& From = Branch.Corners[Index];
            const FVector& To = Branch.Corners[Index + 1];

            if (GenerateLevel(From, To) == false)
            {
                BW_LOG(this, Warning, TEXT("Could not generate branch from %s to %s (with indices %d to %d)!"), *From.ToString(), *To.ToString(), Index, Index + 1);
                Destroy();

                return;
            }
            BW_LOG(this, Log, TEXT("Done generating branch between %s and %s (with indices %d to %d)!"), *From.ToString(), *To.ToString(), Index, Index + 1);
        }
    }
    BW_LOG(this, Log, TEXT("Done generating level. Start building!"));
    // This line will eventually trigger level building on the clients, since ForceFieldsArray is replicated
    ForceFieldsArray = ForceFields.Array();
    BuildLevel();   
    CreateSpawners();
}

void ABWLevelCreator::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    DOREPLIFETIME(ABWLevelCreator, ForceFieldsArray);
}

bool ABWLevelCreator::CheckProperties() const
{
    if (Corners.Num() < 2)
    {
        BW_LOG(this, Error, TEXT("You need to specify at least two corners"));

        return false;
    }

    if (VeinInstancedMesh->GetStaticMesh() == nullptr)
    {
        BW_LOG(this, Error, TEXT("No Static Mesh found on the Instanced Static Mesh component"));

        return false;
    }
    for (const FCorners& Branch : Branches)
    {
        if (Branch.Corners.Num() < 2)
        {
            BW_LOG(this, Error, TEXT("For a branch you need to specify at least two corners"));

            return false;
        }
        if (!Corners.Contains(Branch.Corners[0]))
        {
            BW_LOG(this, Error, TEXT("Branches must start off from a main corner"));

            return false;
        }
    }

    return true;
}

bool ABWLevelCreator::GenerateLevel(const FVector& From, const FVector& To)
{
    int Counter = 0;
    const int Step = 100;
    FVector PreviousPosition = From;
    FVector CurrentPosition = From;
    FVector NextPosition = From;

    // If we are close enough (2 meters) to the destination, then we are done
    while ((CurrentPosition - To).SizeSquared() > 200 * 200 && Counter++ < 100)
    {
        FVector Direction = (To - CurrentPosition).GetSafeNormal() * Step;
        FVector Random = FMath::VRand() * Step;

        Direction = BWVectorUtilities::Round(Direction, Step);
        Random = BWVectorUtilities::Round(Random, Step);
        NextPosition = CurrentPosition + Direction + Random;
        NextPosition = BWVectorUtilities::Round(NextPosition, Step);
        if (ForceFields.Contains(NextPosition))
            continue;
        BWVectorUtilities::Neighbouring(CurrentPosition, 300, std::bind(&ABWLevelCreator::AddToLevel, this, std::placeholders::_1), Step);
        CreateForceField(CurrentPosition, PreviousPosition, NextPosition);
        PreviousPosition = CurrentPosition;
        CurrentPosition = NextPosition;
    }
    // TODO: Handle the remaining part of the path

    // If we managed to reach the destination, return true, false otherwise
    return Counter < 100;
}

void ABWLevelCreator::BuildLevel()
{
    // Just run thrugh the level, and create vein actors on the edge
    for (const FVector& position: Level)
    {
        BWVectorUtilities::Neighbouring(position, 100, std::bind(&ABWLevelCreator::CreateVein, this, std::placeholders::_1), 100);
    }
    BW_LOG(this, Log, TEXT("Done building level! Created %d veins"), VeinInstancedMesh->GetInstanceCount());
    BW_LOG(this, Log, TEXT("Level contains %d element"), Level.Num());
    BW_LOG(this, Log, TEXT("ForceFields contains %d element"), ForceFields.Num());
}

void ABWLevelCreator::CreateSpawners() const
{
    UWorld* World = GetWorld();

    if (nullptr == World)
    {
        return;
    }
    for (const TPair<FCellSpawnerKey, FBWCellSpawnerProperties>& SpawnerProperty : CellSpawners)
    {
        FVector Position = SpawnerProperty.Key.Position;
        UClass* Class = SpawnerProperty.Key.SpawnerClass;
        ABWCellSpawner* Spawner = World->SpawnActor<ABWCellSpawner>(Class, Position, FRotator::ZeroRotator);

        Spawner->SetProperties(SpawnerProperty.Value);
    }
}

void ABWLevelCreator::AddToLevel(const FVector& Position)
{
    if (!Level.Contains(Position))
        Level.Add(Position);
}

void ABWLevelCreator::CreateVein(const FVector& Position)
{
    // Veins are added at the ege of the level.
    // So we only need to create a vein at the given position,
    // if it is not part of the level, and has not yet been added
    // as a vein either
    if (!Level.Contains(Position) && !Vein.Contains(Position))
    {
        FTransform Transform;

        Transform.SetLocation(Position);
        VeinInstancedMesh->AddInstanceWorldSpace(Transform);
        Vein.Add(Position);
    }
}

void ABWLevelCreator::CreateForceField(const FVector& Position,
                                       const FVector& PreviousPosition,
                                       const FVector& NextPosition)
{
    UWorld* World = GetWorld();

    if (World)
    {
        FRotator Rotation(0.0f);
        ABWForceField* ForceField = World->SpawnActor<ABWForceField>(Position, Rotation);
        FVector BloodForce = (NextPosition - Position).GetSafeNormal();
        FVector VirusForce = (PreviousPosition - Position).GetSafeNormal();

        BW_LOG(this, Log, TEXT("Adding ForceField at %s, with BloodForce %s and VirusForce %s"),
               *Position.ToString(), *BloodForce.ToString(), *VirusForce.ToString());
        ForceField->SetForces(BloodForce, VirusForce);
        ForceFields.Add(Position);
    }
}

void ABWLevelCreator::OnRep_ForceFieldsArray()
{
    // Just create the level based on the given ForceFields, and then build the level
    // TODO: We need to make sure, that the ISMes ar created in the same order as on the server
    for (FVector Position : ForceFieldsArray)
        BWVectorUtilities::Neighbouring(Position, 300, std::bind(&ABWLevelCreator::AddToLevel, this, std::placeholders::_1), 100);
    BuildLevel();
}
