#include "BWCell.h"
#include "BloodyWars.h"
#include <Components/StaticMeshComponent.h>
#include <Engine/World.h>

ABWCell::ABWCell()
{
    BW_LOG_METHOD(this);
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    // Set up replication
    SetReplicates(true);
    SetReplicateMovement(true);
    MinNetUpdateFrequency = 50.0f;
    NetUpdateFrequency = 100.0f;
    bNetLoadOnClient = true;
    CellMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cell Mesh"));
    if (nullptr == RootComponent)
    {
        RootComponent = CellMesh;
    }
    else
    {
        CellMesh->SetupAttachment(RootComponent);
    }
    CellMesh->SetSimulatePhysics(true);
    CellMesh->SetEnableGravity(false);
    CellMesh->SetMassOverrideInKg();
    Speed = 100.0f;
    AngularVelocity = FMath::VRand();
}

void ABWCell::SetForce(const FVector& InForce)
{
    Force = InForce;
}

void ABWCell::BeginPlay()
{
    BW_LOG_METHOD(this);
	Super::BeginPlay();
    if (false == CheckProperties())
    {
        Destroy();

        return;
    }
    CellMesh->SetPhysicsAngularVelocityInRadians(AngularVelocity);
}

void ABWCell::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    if (HasAuthority())
    {
        FVector AlignedForce = Force;

        // Make sure we always rotate
        CellMesh->SetPhysicsAngularVelocityInRadians(AngularVelocity);
        // Align force if needed
        AlignedForce = AlignForce(AlignedForce, FVector::UpVector);
        AlignedForce = AlignForce(AlignedForce, -FVector::UpVector);
        AlignedForce = AlignForce(AlignedForce, FVector::RightVector);
        AlignedForce = AlignForce(AlignedForce, -FVector::RightVector);
        AlignedForce = AlignForce(AlignedForce, FVector::ForwardVector);
        AlignedForce = AlignForce(AlignedForce, -FVector::ForwardVector);
        // IF we cannot move to the given direction, try some random direction
        if (AlignedForce == FVector::ZeroVector)
        {
            AlignedForce = FMath::VRand();
        }
        // Keep adding force
        CellMesh->AddForce(AlignedForce * Speed);
    }
}

bool ABWCell::CheckProperties() const
{
    if (nullptr == CellMesh->GetStaticMesh())
    {
        BW_LOG(this, Error, TEXT("No Static Mesh set!"));

        return false;
    }
    if (false == CellMesh->IsSimulatingPhysics())
    {
        BW_LOG(this, Error, TEXT("Static Mesh Component has to simulate physics!"));

        return false;
    }
    if (CellMesh->IsGravityEnabled())
    {
        BW_LOG(this, Error, TEXT("Graviti should not be enabled on the Static Mesh Component!"));

        return false;
    }
    if (0 == Speed)
    {
        BW_LOG(this, Error, TEXT("Speed is set to Zero, this Cell will not move!"));

        return false;
    }

    return true;
}

FVector ABWCell::AlignForce(const FVector& InForce, const FVector& InDirection) const
{
    UWorld* World = GetWorld();
    FVector Check = InForce * InDirection;

    if (!World)
    {
        return InForce;
    }
    // Check if InForce has any component towards InDirection
    if (Check.X > 0 || Check.Y > 0 || Check.Z > 0)
    {
        FHitResult HitResult;
        // TODO: Use a property for how far we should see ahead
        FVector End = GetActorLocation() + InDirection * 150;
        
        // TODO: Use our own channel, to check this trace only agains veins - or?
        if (!World->LineTraceSingleByChannel(HitResult, GetActorLocation(), End, ECC_Visibility))
        {
            // There is nothing in our way, we can go ahead
            return InForce;
        }

        // We got a hit, so it is better to align the Force to "slide" on whatever we have found
        return FVector::VectorPlaneProject(InForce, HitResult.ImpactNormal).GetSafeNormal();
    }

    return InForce;
}
