// Fill out your copyright notice in the Description page of Project Settings.
#include "BloodyWars.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BloodyWars, "BloodyWars" );

DEFINE_LOG_CATEGORY(LogBW);
